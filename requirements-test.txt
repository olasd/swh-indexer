confluent-kafka
hypothesis >= 3.11.0
pytest
pytest-mock
swh.core[testing]
swh.storage[testing] >= 0.10.0

types-click
types-pyyaml
